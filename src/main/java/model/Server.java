package model;

import util.UserThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class Server {

    private final int port;
    private final Set<String> userNames = new HashSet<>();
    private final Set<UserThread> userThreads = new HashSet<>();
    private boolean isChatNew = true;

    public Server(int port) {
        this.port = port;
    }

    public void execute() {
        try (ServerSocket server = new ServerSocket(port)){
            System.out.println("Chat's server is listening on port: " + port);
            while (isChatNew || !userThreads.isEmpty()) {
                Socket client = server.accept();
                System.out.println("New user connected.");
                UserThread newUser = new UserThread(client, this);
                userThreads.add(newUser);
                newUser.start();
                isChatNew = false;
            }
        } catch (IOException e) {
            System.err.println("Server's error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Syntax: java <app-name> <port-number>");
            System.exit(0);
        }
        int port = Integer.parseInt(args[0]);
        Server server = new Server(port);
        server.execute();
    }

    public void broadcast(String mess, UserThread excludeUser) {
        for (UserThread user : userThreads) {
            if (!user.equals(excludeUser)) {
                user.sendMessage(mess);
                System.out.println(mess);
            }
        }
    }

    public void addUserName(String userName) {
        userNames.add(userName);
    }

    public void removeUser(String userName, UserThread user) {
        boolean removed = userNames.remove(userName);
        if (removed) {
            userThreads.remove(user);
            System.out.println("User " + userName + "quited.");
        }
    }

    public Set<String> getUserNames() {
        return this.userNames;
    }

    public boolean hasUsers() {
        return !this.userNames.isEmpty();
    }
}
