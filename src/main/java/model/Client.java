package model;

import util.ReadThread;
import util.WriteThread;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    private final String hostName;
    private final int port;
    private String userName;

    public Client(String hostName, int port) {
        this.hostName = hostName;
        this.port = port;
    }

    public void execute() {
        try {
            Socket socket = new Socket(hostName, port);
            System.out.println("Connected to the chat server.");
            new ReadThread(socket, this).start();
            new WriteThread(socket, this).start();
        } catch (UnknownHostException e) {
            System.err.println("Server not found: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public static void main(String[] args) {
        if (args.length < 2) return;

        String hostName = args[0];
        int port = Integer.parseInt(args[1]);

        Client client = new Client(hostName, port);
        client.execute();
    }
}
