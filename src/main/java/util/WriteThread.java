package util;

import model.Client;

import java.io.Console;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class WriteThread extends Thread {
    private PrintWriter writer;
    private final Socket socket;
    private final Client client;

    public WriteThread(Socket socket, Client client) {
        this.socket = socket;
        this.client = client;

        try {
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
        } catch (IOException e) {
            System.err.println("Error getting output stream in WriteThread: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        Console console = System.console();

        String userName = console.readLine("Enter your name: ");
        client.setUserName(userName);
//        writer.println(userName);
        String text;
        do {
            text = console.readLine(userName + ": ");
            if (text.charAt(0) != '/') {
                writer.println(text.trim().replace("\n", ""));
            } else {
                writer.println("");
            }
        } while (!text.equals("/exit"));

        try {
            socket.close();
        } catch (IOException e) {
            System.err.println("Error writing to server: " + e.getMessage());
        }
    }
}
