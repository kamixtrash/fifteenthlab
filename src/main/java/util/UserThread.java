package util;

import model.Server;

import java.io.*;
import java.net.Socket;

public class UserThread extends Thread {
    private final Socket socket;
    private final Server server;
    private PrintWriter writer;

    public UserThread(Socket socket, Server server) {
        this.socket = socket;
        this.server = server;
    }

    @Override
    public void run() {
        try {
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
            printUsers();
            String userName = reader.readLine();
            server.addUserName(userName);
            String serverMess = "SYSTEM: New user connected: " + userName;
            server.broadcast(serverMess, this);
            String clientMess;

            do {
                clientMess = reader.readLine();
                serverMess = userName + ": " + clientMess;
                server.broadcast(serverMess, this);
            } while (!clientMess.equals("/exit"));

            server.removeUser(userName, this);
            socket.close();
            serverMess = "SYSTEM: " + userName + " has quited.";
            server.broadcast(serverMess, this);
        } catch (IOException e) {
            System.err.println("Error in UserThread: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void printUsers() {
        if (server.hasUsers()) {
            writer.println("Connected users: " + server.getUserNames());
            return;
        }
        writer.println("No other users connected.");
    }

    public void sendMessage(String message) {
        writer.println(message);
    }
}
